package com.g18;

import com.g18.controller.MainController;
import com.g18.model.Television;



public class Application {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Television samsungTv = new Television("Samsung EU595442");
		
		MainController mainController = new MainController(samsungTv);

	}
	
	
	
}
