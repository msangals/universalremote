package com.g18.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.management.MemoryNotificationInfo;

import com.g18.command.VolumeDownCommand;
import com.g18.command.VolumeUpCommand;
import com.g18.interfaces.Command;
import com.g18.model.Television;
import com.g18.model.UniversalRemoteControl;
import com.g18.statics.ControllStatics;
import com.g18.view.MainView;



public class MainController implements ActionListener {

	private MainView mainView;
	private UniversalRemoteControl universalRemoteControl;
	private Television television;
	
	public MainController(Television tv) {
		
		//Model
		television = tv;
		
		//View
		mainView = new MainView(this);
		
		
		
		//Commands
		Command volumeUpCommand = new VolumeUpCommand(television);
		Command volumeDownCommand = new VolumeDownCommand(television);
		
		universalRemoteControl = new UniversalRemoteControl(volumeUpCommand, volumeDownCommand);
		
		//Add Observer to model
		this.television.addObserver(mainView);
		mainView.setVisible(true);
		

	}
	

	@Override
	public void actionPerformed(ActionEvent event) {
		
		System.out.println("ACTION: " + event.getActionCommand().toString());
		
        if(event.getActionCommand().equalsIgnoreCase(ControllStatics.ACTION_VOLUMEN_UP))
        {
        	this.universalRemoteControl.turnUpVolumen();
        }
        
        if(event.getActionCommand().equalsIgnoreCase(ControllStatics.ACTION_VOLUMEN_DOWN))
        {
        	this.universalRemoteControl.turnDownVolumen();
        }
        
        if(event.getActionCommand().equalsIgnoreCase(ControllStatics.ACTION_PROGRAMM_UP))
        {
        	//Hier wird die Action vom Button "Programm +" aufgerufen
        }
        
        if(event.getActionCommand().equalsIgnoreCase(ControllStatics.ACTION_PROGRAMM_DOWN))
        {
        	//Hier wird die Action vom Button "Programm -" aufgerufen
        }   
		
	}

}
