package com.g18.view;

import java.awt.BorderLayout;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSplitPane;

import com.g18.controller.MainController;
import com.g18.interfaces.TelevisionConfig;
import com.g18.model.Television;
import com.g18.statics.ControllStatics;


public class MainView extends JFrame implements Observer {
	
	private static final long serialVersionUID = 1L;
	
	private MainController mainController;
	
	private JLabel lblVolumen, lblProgram, lblVolumenView, lblProgramView;
	
	public MainView(MainController mC) {
		
		mainController = mC;

		setSize(400, 400);
		setVisible(true);
		
		setLayout(new BorderLayout());
		
		
		
		JButton btnVolumenUp = new JButton("Volumen +");
		JButton btnVolumenDown = new JButton("Volumen -");
		JButton btnProgrammUp = new JButton("Programm +");
		JButton btnProgrammDown = new JButton("Programm -");
		
		
		btnVolumenUp.addActionListener(mainController);
		btnVolumenUp.setActionCommand(ControllStatics.ACTION_VOLUMEN_UP);
		
		btnVolumenDown.addActionListener(mainController);
		btnVolumenDown.setActionCommand(ControllStatics.ACTION_VOLUMEN_DOWN);
		
		btnProgrammUp.addActionListener(mainController);
		btnProgrammUp.setActionCommand(ControllStatics.ACTION_PROGRAMM_UP);
		
		btnProgrammDown.addActionListener(mainController);
		btnProgrammDown.setActionCommand(ControllStatics.ACTION_PROGRAMM_DOWN);
		
		
		JPanel leftPanel = new JPanel();
		
		leftPanel.add(btnVolumenDown);
		leftPanel.add(btnVolumenUp);
		
		leftPanel.add(btnProgrammDown);
		leftPanel.add(btnProgrammUp);
		
		
		
		lblVolumen = new JLabel("Volumen: ");
		lblVolumenView = new JLabel(Integer.toString(TelevisionConfig.INITIAL_VOLUMEN));
		lblProgram = new JLabel("Programm: ");
		lblProgramView = new JLabel(Integer.toString(TelevisionConfig.INITIAL_PROGRAM));
		
		
		
		JPanel rightPanel = new JPanel();
		
		rightPanel.add(lblVolumen);
		rightPanel.add(lblVolumenView);
		rightPanel.add(lblProgram);
		rightPanel.add(lblProgramView);
		
		
		
		JSplitPane pane = new JSplitPane( JSplitPane.HORIZONTAL_SPLIT, leftPanel, rightPanel);
		pane.setDividerLocation(200);
		getContentPane().add(pane);

	}



	@Override
	protected void frameInit() {

		super.frameInit();
		setDefaultCloseOperation(javax.swing.JFrame.EXIT_ON_CLOSE);

	}



	@Override
	public void update(Observable obj, Object arg) {
		
		System.out.println(obj.getClass());
		
		if(obj instanceof Television){
			
			this.lblVolumenView.setText(Integer.toString(((Television) obj).getCurrentVolumen()));
			this.lblProgramView.setText(Integer.toString(((Television) obj).getCurrentProgram()));
			
			System.out.println("Volumen wurde gesetzt");
        }
		
	}

}
