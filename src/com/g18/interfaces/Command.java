package com.g18.interfaces;

public interface Command {
	public void execute();
}
