package com.g18.model;

import java.util.Observable;

import com.g18.interfaces.Command;

public class UniversalRemoteControl {

	private Command volumenDownCommand;
	private Command volumenUpCommand;

	public UniversalRemoteControl(Command volumenUp, Command volumenDown) {

		volumenDownCommand = volumenDown;
		volumenUpCommand = volumenUp;

	}

	/**Hier wird der VolumenDown-Commander aufgerufen**/
	public void turnDownVolumen() {
		volumenDownCommand.execute();
	}

	/**Hier wird der VolumenUp-Commander aufgerufen**/
	public void turnUpVolumen() {
		volumenUpCommand.execute();
	}

	
	/**Es k�nnen noch keine Fernsehprogramme gewechselt werden**/
	
	
}
