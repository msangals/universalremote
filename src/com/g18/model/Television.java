package com.g18.model;

import java.util.Observable;

import com.g18.interfaces.TelevisionConfig;

public class Television extends Observable implements TelevisionConfig {

	public String type;
	public int volumen;
	private int program;
	
	public Television(String type) {
		this.type = type;
		setInitialVolumen();
		setInitialProgram();
	}
	
	public void volumenUp() {
		if(volumen < 100) {
			volumen++;
			triggerChange();
		}
	}
	
	public void volumenDown() {
		if(volumen > 0) {
			volumen--;
			triggerChange();
		}
	}
	
	public void programUp() {
		if(program == 99) {
			program = 0;
		}
		else
		{
			program++;
		}	 
	}
	
	public void programDown() {
		if(program == 0) {
			program = 99;
		}
		else
		{
			program--;
		}	 
	}
	
	private void setInitialVolumen() {
		this.volumen = INITIAL_VOLUMEN;
	}
	
	private void setInitialProgram() {
		this.program = INITIAL_PROGRAM;
	}
	
	public void setVolumen(int volumen) {
		this.volumen = volumen;
	}
	
	public void setProgram(int program) {
		this.program = program;
	}
	
	public int getCurrentVolumen() {
		return volumen;
	}
	
	public int getCurrentProgram() {
		return program;
	}
	
	private void triggerChange() {
        setChanged();
        notifyObservers();
	}

}
