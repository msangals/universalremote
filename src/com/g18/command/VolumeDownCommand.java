package com.g18.command;

import com.g18.interfaces.Command;
import com.g18.model.Television;

public class VolumeDownCommand implements Command {

	private Television television;
	
	public VolumeDownCommand(Television television) {
		this.television = television;
	}
	
	public void execute() {
		television.volumenDown();
	}



}
