package com.g18.command;

import com.g18.interfaces.Command;
import com.g18.model.Television;

public class VolumeUpCommand implements Command {
	
private Television television;
	
	public VolumeUpCommand(Television television) {
		this.television = television;
	}
	
	@Override
	public void execute() {
		television.volumenUp();
	}


}
